
var href = window.location.pathname.split('/');
var n_href = href.splice(-1)[0];
if (n_href == '/' || n_href == '/index.html' || n_href == '' || n_href == 'index.html') {
    $("#header").removeClass('header-otherpage');
    $("#gnav").removeClass("js-gnav");
}
else {
    $("#header").addClass('header-otherpage');
    $("#gnav").addClass("js-gnav");
}

$(window).scroll(function () {
    if ($(this).scrollTop() > 400) {
        $('.footer-backtop-btn').fadeIn();
    }
    if ($(this).scrollTop() < 400) {
        $('.footer-backtop-btn').fadeOut();
    }
    if ($(this).scrollTop() > $("#header").height()) {
        $('#header').addClass('js-header');
    }
    else if ($(this).scrollTop() < $("#header").height()) {
        $('#header').removeClass('js-header');
        $('#header').removeAttr("style");
    }
    if ($(this).scrollTop() > $("#keyvisual").height() - 10) {
        $('.js-header').css({ "transform": "translateY(0)", "position": "fixed", "transition": "0.3s ease-out" });
    }
});